-- Database: barbiere_nicolas_info1c_sitedemontre_104_2021
-- Détection si une autre base de donnée du même nom_montre existe

DROP DATABASE IF EXISTS barbiere_nicolas_info1c_sitedemontre_104_2021;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS barbiere_nicolas_info1c_sitedemontre_104_2021;

-- Utilisation de cette base de donnée

USE barbiere_nicolas_info1c_sitedemontre_104_2021;

-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Lun 31 Mai 2021 à 09:51
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `barbiere_nicolas_info1c_sitedemontre_104_2021`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_materiau`
--

CREATE TABLE `t_materiau` (
  `id_materiau` int(11) NOT NULL,
  `materiau` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_materiau`
--

INSERT INTO `t_materiau` (`id_materiau`, `materiau`) VALUES
(28, 'Acier'),
(27, 'Argent'),
(29, 'céramique'),
(34, 'diament'),
(36, 'emeraude'),
(26, 'or'),
(31, 'or blanc'),
(32, 'Or Rose'),
(37, 'Oystersteel'),
(39, 'Rubis'),
(35, 'saphir'),
(38, 'Titane');

-- --------------------------------------------------------

--
-- Structure de la table `t_montre`
--

CREATE TABLE `t_montre` (
  `id_montre` int(11) NOT NULL,
  `nom_montre` varchar(255) DEFAULT NULL,
  `marque_montre` varchar(255) DEFAULT NULL,
  `description_montre` text,
  `photo_montre` text,
  `prix_montre` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_montre`
--

INSERT INTO `t_montre` (`id_montre`, `nom_montre`, `marque_montre`, `description_montre`, `photo_montre`, `prix_montre`) VALUES
(2, 'Submariner', 'Rolex', 'Une montres qui illustrent les liens historiques entre Rolex et le monde de la plongée. Les deux garde-temps arborent désormais un boîtier redessiné et légèrement plus grand de 41 mm dont le profil est souligné par les reflets lumineux des côtés du boîtier et des cornes et sont montés sur un bracelet remodelé.', 'https://content.rolex.com/dam/2021/upright-bba-with-shadow/m126610ln-0001.png?imwidth=270', '7\'700'),
(3, 'SpeedMaster MoonWhatch Professional', 'Omega', 'Utilisée lors des six missions lunaires, la légendaire Speedmaster incarne à la perfection l\'esprit pionnier et aventurier de la marque.\r\n\r\nCe modèle est pourvu d\'un cadran noir avec verre hésalite, ponctué d\'un compteur de petite seconde, d\'un compteur des 30 minutes et d\'un compteur des 12 heures, sans oublier l\'aiguille centrale du chronographe. La lunette noire, munie d\'une échelle tachymétrique, est montée sur un boîtier de 42 mm en acier inoxydable accompagné d\'un bracelet assorti.', 'https://www.omegawatches.com/media/catalog/product/cache/a5c37fddc1a529a1a44fea55d527b9a116f3738da3a2cc38006fcc613c37c391/o/m/omega-speedmaster-moonwatch-31130423001005-l.png', '4\'950');

-- --------------------------------------------------------

--
-- Structure de la table `t_montre_materiau`
--

CREATE TABLE `t_montre_materiau` (
  `id_montre_materiau` int(11) NOT NULL,
  `fk_montre` int(11) DEFAULT NULL,
  `fk_materiau` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_montre_materiau`
--

INSERT INTO `t_montre_materiau` (`id_montre_materiau`, `fk_montre`, `fk_materiau`) VALUES
(9, 2, 37),
(18, 3, 28);

-- --------------------------------------------------------

--
-- Structure de la table `t_montre_nombre`
--

CREATE TABLE `t_montre_nombre` (
  `id_montre_nombre` int(11) NOT NULL,
  `fk_montre` int(11) DEFAULT NULL,
  `fk_nombre` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_nombre`
--

CREATE TABLE `t_nombre` (
  `id_nombre` int(11) NOT NULL,
  `nombre` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_materiau`
--
ALTER TABLE `t_materiau`
  ADD PRIMARY KEY (`id_materiau`),
  ADD UNIQUE KEY `materiau` (`materiau`);

--
-- Index pour la table `t_montre`
--
ALTER TABLE `t_montre`
  ADD PRIMARY KEY (`id_montre`);

--
-- Index pour la table `t_montre_materiau`
--
ALTER TABLE `t_montre_materiau`
  ADD PRIMARY KEY (`id_montre_materiau`),
  ADD KEY `fk_montre` (`fk_montre`),
  ADD KEY `fk_materiau` (`fk_materiau`);

--
-- Index pour la table `t_montre_nombre`
--
ALTER TABLE `t_montre_nombre`
  ADD PRIMARY KEY (`id_montre_nombre`);

--
-- Index pour la table `t_nombre`
--
ALTER TABLE `t_nombre`
  ADD PRIMARY KEY (`id_nombre`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_materiau`
--
ALTER TABLE `t_materiau`
  MODIFY `id_materiau` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT pour la table `t_montre`
--
ALTER TABLE `t_montre`
  MODIFY `id_montre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `t_montre_materiau`
--
ALTER TABLE `t_montre_materiau`
  MODIFY `id_montre_materiau` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT pour la table `t_montre_nombre`
--
ALTER TABLE `t_montre_nombre`
  MODIFY `id_montre_nombre` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_nombre`
--
ALTER TABLE `t_nombre`
  MODIFY `id_nombre` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_montre_materiau`
--
ALTER TABLE `t_montre_materiau`
  ADD CONSTRAINT `t_montre_materiau_ibfk_1` FOREIGN KEY (`fk_materiau`) REFERENCES `t_materiau` (`id_materiau`),
  ADD CONSTRAINT `t_montre_materiau_ibfk_2` FOREIGN KEY (`fk_montre`) REFERENCES `t_montre` (`id_montre`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

