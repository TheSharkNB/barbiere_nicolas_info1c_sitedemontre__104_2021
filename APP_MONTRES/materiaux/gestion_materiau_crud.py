import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_MONTRES import obj_mon_application
from APP_MONTRES.database.connect_db_context_manager import MaBaseDeDonnee
from APP_MONTRES.erreurs.exceptions import *
from APP_MONTRES.erreurs.msg_erreurs import *
from APP_MONTRES.materiaux.gestion_materiaux_wtf_forms import FormWTFAjouterMateriaux
from APP_MONTRES.materiaux.gestion_materiaux_wtf_forms import FormWTFDeleteMateriaux
from APP_MONTRES.materiaux.gestion_materiaux_wtf_forms import FormWTFUpdateMateriaux

@obj_mon_application.route("/materiau_afficher/<string:order_by>/<int:id_materiau_sel>", methods=['GET', 'POST'])
def materiau_afficher(order_by, id_materiau_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion materiaux ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionGenres {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_materiau_sel == 0:
                    strsql_materiau_afficher = """SELECT id_materiau, materiau FROM t_materiau ORDER BY id_materiau ASC"""
                    mc_afficher.execute(strsql_materiau_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_genre"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du genre sélectionné avec un nom_montre de variable
                    valeur_id_materiau_selected_dictionnaire = {"value_id_materiau_selected": id_materiau_sel}
                    strsql_materiau_afficher = """SELECT id_materiau, materiau FROM t_materiau WHERE id_materiau = %(value_id_materiau_selected)s"""

                    mc_afficher.execute(strsql_materiau_afficher, valeur_id_materiau_selected_dictionnaire)
                else:
                    strsql_materiau_afficher = """SELECT id_materiau, materiau FROM t_materiau ORDER BY id_materiau DESC"""

                    mc_afficher.execute(strsql_materiau_afficher)

                data_materiau = mc_afficher.fetchall()

                print("data_materiau ", data_materiau, " Type : ", type(data_materiau))

                # Différencier les messages si la table est vide.
                if not data_materiau and id_materiau_sel == 0:
                    flash("""La table "t_materiau" est vide. !!""", "warning")
                elif not data_materiau and id_materiau_sel > 0:
                    # Si l'utilisateur change l'id_genre dans l'URL et que le genre n'existe pas,
                    flash(f"Le genre demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_genre" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Matériau(x) affiché(s) !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. materiau_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} materiau_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("Materiaux/materiau_afficher.html", data=data_materiau)



@obj_mon_application.route("/materiau_ajouter", methods=['GET', 'POST'])
def materiau_ajouter_wtf():
    form = FormWTFAjouterMateriaux()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion materiaux ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionGenres {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                name_materiau_wtf = form.nom_materiau_wtf.data

                name_materiau = name_materiau_wtf
                valeurs_insertion_dictionnaire = {"value_materiau": name_materiau}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_materiau = """INSERT INTO t_materiau (id_materiau, materiau) VALUES (NULL,%(value_materiau)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_materiau, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('materiau_afficher', order_by='ASC', id_materiau_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_genre_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_genre_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion materiaux CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("Materiaux/materiau_ajouter_wtf.html", form=form)


@obj_mon_application.route("/materiau_update", methods=['GET', 'POST'])
def materiau_update_wtf():
    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_genre"
    id_materiau_update = request.values['id_materiau_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateMateriaux()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "materiau_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            name_materiau_update = form_update.nom_materiau_update_wtf.data

            valeur_update_dictionnaire = {"value_id_materiau": id_materiau_update, "value_name_materiau": name_materiau_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_materiau = """UPDATE t_materiau SET materiau = %(value_name_materiau)s WHERE id_materiau = %(value_id_materiau)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_materiau, valeur_update_dictionnaire)

            flash(f"Modifications éffectuées !", "success")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_materiau_update"
            return redirect(url_for('materiau_afficher', order_by="ASC", id_materiau_sel=0))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_genre" et "intitule_genre" de la "t_genre"
            str_sql_id_materiau = "SELECT id_materiau, materiau FROM t_materiau WHERE id_materiau = %(value_id_materiau)s"
            valeur_select_dictionnaire = {"value_id_materiau": id_materiau_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_materiau, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom_montre genre" pour l'UPDATE
            data_nom_materiau = mybd_curseur.fetchone()
            print("data_nom_materiau ", data_nom_materiau, " type ", type(data_nom_materiau), " materiau ",
                  data_nom_materiau["materiau"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "materiau_update_wtf.html"
            form_update.nom_materiau_update_wtf.data = data_nom_materiau["materiau"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans materiau_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans materiau_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans materiau_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans materiau_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("Materiaux/materiau_update_wtf.html", form_update=form_update)


@obj_mon_application.route("/materiau_delete", methods=['GET', 'POST'])
def materiau_delete_wtf():
    data_montre_attribue_materiau_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_genre"
    id_materiau_delete = request.values['id_materiau_btn_delete_html']

    # Objet formulaire pour effacer le genre sélectionné.
    form_delete = FormWTFDeleteMateriaux()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("materiau_afficher", order_by="ASC", id_materiau_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "materiaux/materiau_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_montre_attribue_materiau_delete = session['data_montre_attribue_materiau_delete']
                print("data_montre_attribue_materiau_delete ", data_montre_attribue_materiau_delete)

                flash(f"Effacer le genre de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer genre" qui va irrémédiablement EFFACER le genre
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_materiau": id_materiau_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_films_genre = """DELETE FROM t_montre_materiau WHERE fk_materiau = %(value_id_materiau)s"""
                str_sql_delete_id_materiau = """DELETE FROM t_materiau WHERE id_materiau = %(value_id_materiau)s"""
                # Manière brutale d'effacer d'abord la "fk_genre", même si elle n'existe pas dans la "t_genre_film"
                # Ensuite on peut effacer le genre vu qu'il n'est plus "lié" (INNODB) dans la "t_genre_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_films_genre, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_id_materiau, valeur_delete_dictionnaire)

                flash(f"matériau définitivement effacé !!", "success")
                print(f"matériau définitivement effacé !!")

                # afficher les données
                return redirect(url_for('materiau_afficher', order_by="ASC", id_materiau_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_materiau": id_materiau_delete}
            print(id_materiau_delete, type(id_materiau_delete))

            # Requête qui affiche tous les montres_materiaux qui ont le genre que l'utilisateur veut effacer
            str_sql_genres_films_delete = """SELECT id_montre_materiau, nom_montre, id_materiau, materiau FROM t_montre_materiau
                                            INNER JOIN t_montre ON t_montre_materiau.fk_montre = t_montre.id_montre
                                            INNER JOIN t_materiau ON t_montre_materiau.fk_materiau = t_materiau.id_materiau
                                            WHERE fk_materiau = %(value_id_materiau)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_genres_films_delete, valeur_select_dictionnaire)
            data_montre_attribue_materiau_delete = mybd_curseur.fetchall()
            print("data_montre_attribue_materiau_delete...", data_montre_attribue_materiau_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "materiaux/materiau_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_montre_attribue_materiau_delete'] = data_montre_attribue_materiau_delete

            # Opération sur la BD pour récupérer "id_genre" et "intitule_genre" de la "t_genre"
            str_sql_id_materiau = "SELECT id_materiau, materiau FROM t_materiau WHERE id_materiau = %(value_id_materiau)s"

            mybd_curseur.execute(str_sql_id_materiau, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom_montre genre" pour l'action DELETE
            data_nom_materiau = mybd_curseur.fetchone()
            print("data_nom_materiau ", data_nom_materiau, " type ", type(data_nom_materiau), " materiau ",
                  data_nom_materiau["materiau"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "materiau_delete_wtf.html"
            form_delete.nom_materiau_delete_wtf.data = data_nom_materiau["materiau"]

            # Le bouton pour l'action "DELETE" dans le form. "materiau_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans materiau_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans materiau_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans materiau_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans materiau_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("Materiaux/materiau_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_montres_associes=data_montre_attribue_materiau_delete)


