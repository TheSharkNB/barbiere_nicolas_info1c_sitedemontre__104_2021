"""
    Fichier : gestion_materiaux_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterMateriaux(FlaskForm):
    """
        Dans le formulaire "materiau_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_materiau_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    nom_materiau_wtf = StringField("Clavioter le matériau ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                         Regexp(nom_materiau_regexp,
                                                                          message="Pas de chiffres, de caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace à double, de double "
                                                                                  "apostrophe, de double trait union")
                                                                         ])
    submit = SubmitField("Enregistrer materiau")


class FormWTFUpdateMateriaux(FlaskForm):
    """
        Dans le formulaire "materiau_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_materiau_update_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    nom_materiau_update_wtf = StringField("Clavioter le matériau ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                             Regexp(nom_materiau_update_regexp,
                                                                                 message="Pas de chiffres, de "
                                                                                         "caractères "
                                                                                         "spéciaux, "
                                                                                         "d'espace à double, de double "
                                                                                         "apostrophe, de double trait "
                                                                                         "union")
                                                                             ])
    submit = SubmitField("Update materiau")


class FormWTFDeleteMateriaux(FlaskForm):
    """
        Dans le formulaire "materiau_delete_wtf.html"

        nom_materiau_delete_wtf : Champ qui reçoit la valeur du genre, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "genre".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_genre".
    """
    nom_materiau_delete_wtf = StringField("Effacer ce matériau")
    submit_btn_del = SubmitField("Effacer matériau")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
