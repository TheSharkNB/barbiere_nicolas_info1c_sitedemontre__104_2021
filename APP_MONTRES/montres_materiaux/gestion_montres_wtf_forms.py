
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterMontres(FlaskForm):

    nom_montre_wtf = StringField("Saisisez le nom ", validators=[Length(min=2, max=60, message="min 2 max 60"),

                                                                         ])

    marque_montre_wtf = StringField("La marque", validators=[Length(min=2, max=50, message="min 2 max 50"),

                                                                         ])
    description_montre_wtf = StringField("La description ",
                                     validators=[Length(min=2, max=500, message="min 2 max 500"),

                                                 ])

    photo_montre_wtf = StringField("L'url de la photo",
                                     validators=[Length(min=2, message="min 2"),

                                                 ])
    prix_montre_regexp = "[0-9]|[.']+$"
    prix_montre_wtf = StringField("Le prix ",
                                     validators=[Length(min=2, max=30, message="min 2 max 30"),
                                                 Regexp(prix_montre_regexp,
                                                        message="Pas de lettres !")
                                                 ])


    submit = SubmitField("Enregistrer la montre")


class FormWTFUpdateMontres(FlaskForm):

    nom_montre_update_wtf = StringField("Saisisez le nom ", validators=[Length(min=2, max=60, message="min 2 max 60"),

                                                                 ])

    marque_montre_update_wtf = StringField("La marque", validators=[Length(min=2, max=50, message="min 2 max 50"),

                                                             ])
    description_montre_update_wtf = StringField("La description ",
                                         validators=[Length(min=2, max=500, message="min 2 max 500"),

                                                     ])

    photo_montre_update_wtf = StringField("L'url de la photo",
                                   validators=[Length(min=2, message="min 2"),

                                               ])
    prix_montre_regexp = "[0-9]|[.']+$"
    prix_montre_update_wtf = StringField("Le prix ",
                                  validators=[Length(min=2, max=30, message="min 2 max 30"),
                                              Regexp(prix_montre_regexp,
                                                     message="Pas de lettres !")
                                              ])
    submit = SubmitField("Update montre")


class FormWTFDeleteMontres(FlaskForm):

    nom_montre_delete_wtf = StringField("Effacer cette montre ?")
    submit_btn_del = SubmitField("Effacer montre")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")

class FormWTFNombreMontres(FlaskForm):

        nombre_montre_regexp = "[0-9]|[']+$"
        nombre_montre_wtf = StringField("Saisisez le nombre disponible ",
                                            validators=[Length(min=1, max=1000000, message="min 1 max 1000000"),
                                                        Regexp(nombre_montre_regexp,
                                                               message="Pas de lettres !")
                                                        ])
