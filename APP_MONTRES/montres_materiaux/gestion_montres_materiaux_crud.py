import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_MONTRES import obj_mon_application
from APP_MONTRES.database.connect_db_context_manager import MaBaseDeDonnee
from APP_MONTRES.erreurs.exceptions import *
from APP_MONTRES.erreurs.msg_erreurs import *
from APP_MONTRES.montres_materiaux.gestion_montres_wtf_forms import FormWTFAjouterMontres
from APP_MONTRES.montres_materiaux.gestion_montres_wtf_forms import FormWTFUpdateMontres
from APP_MONTRES.montres_materiaux.gestion_montres_wtf_forms import FormWTFDeleteMontres
from APP_MONTRES.montres_materiaux.gestion_montres_wtf_forms import FormWTFNombreMontres


@obj_mon_application.route("/montre_materiau_afficher/<int:id_montre_sel>", methods=['GET', 'POST'])
def montre_materiau_afficher(id_montre_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as Exception_init_montre_materiau_afficher:
                code, msg = Exception_init_montre_materiau_afficher.args
                flash(f"{error_codes.get(code, msg)} ", "danger")
                flash(f"Exception _init_montre_materiau_afficher problème de connexion BD : {sys.exc_info()[0]} "
                      f"{Exception_init_montre_materiau_afficher.args[0]} , "
                      f"{Exception_init_montre_materiau_afficher}", "danger")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_materiau_montre_afficher_data = """SELECT id_montre, nom_montre, marque_montre, description_montre, photo_montre, prix_montre,
                                                            GROUP_CONCAT(materiau) as MateriauMontre FROM t_montre_materiau
                                                            RIGHT JOIN t_montre ON t_montre.id_montre = t_montre_materiau.fk_montre
                                                            LEFT JOIN t_materiau ON t_materiau.id_materiau = t_montre_materiau.fk_materiau
                                                            GROUP BY id_montre"""
                if id_montre_sel == 0:
                    # le paramètre 0 permet d'afficher tous les films
                    # Sinon le paramètre représente la valeur de l'id du film
                    mc_afficher.execute(strsql_materiau_montre_afficher_data)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom_montre de variable
                    valeur_id_montre_selected_dictionnaire = {"value_id_montre_selected": id_montre_sel}
                    # En MySql l'instruction HAVING fonctionne comme un WHERE... mais doit être associée à un GROUP BY
                    # L'opérateur += permet de concaténer une nouvelle valeur à la valeur de gauche préalablement définie.
                    strsql_materiau_montre_afficher_data += """ HAVING id_montre= %(value_id_montre_selected)s"""

                    mc_afficher.execute(strsql_materiau_montre_afficher_data, valeur_id_montre_selected_dictionnaire)

                # Récupère les données de la requête.
                data_materiau_montre_afficher = mc_afficher.fetchall()
                print("data_materiau ", data_materiau_montre_afficher, " Type : ", type(data_materiau_montre_afficher))

                # Différencier les messages.
                if not data_materiau_montre_afficher and id_montre_sel == 0:
                    flash("""La table "t_montre" est vide. !""", "warning")
                elif not data_materiau_montre_afficher and id_montre_sel > 0:
                    # Si l'utilisateur change l'id_film dans l'URL et qu'il ne correspond à aucun film
                    flash(f"La montre {id_montre_sel} demandé n'existe pas !!", "warning")
                else:
                    flash(f"Montre(s) et matériau(x) associé(s) affiché(s)", "success")

        except Exception as Exception_montre_materiau_afficher:
            code, msg = Exception_montre_materiau_afficher.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception montre_materiau_afficher : {sys.exc_info()[0]} "
                  f"{Exception_montre_materiau_afficher.args[0]} , "
                  f"{Exception_montre_materiau_afficher}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template("Montres_materiaux/montre_materiau_afficher.html", data=data_materiau_montre_afficher)


@obj_mon_application.route("/edit_materiau_montre_selected", methods=['GET', 'POST'])
def edit_materiau_montre_selected():
    if request.method == "GET":
        try:
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_materiau_afficher = """SELECT id_materiau, materiau FROM t_materiau ORDER BY id_materiau ASC"""
                mc_afficher.execute(strsql_materiau_afficher)
            data_materiau_all = mc_afficher.fetchall()
            print("dans edit_materiau_montre_selected ---> data_materiau_all", data_materiau_all)

            # Récupère la valeur de "id_film" du formulaire html "montre_materiau_afficher.html"
            # l'utilisateur clique sur le bouton "Modifier" et on récupère la valeur de "id_film"
            # grâce à la variable "id_montre_materiau_edit_html" dans le fichier "montre_materiau_afficher.html"
            # href="{{ url_for('edit_materiau_montre_selected', id_montre_materiau_edit_html=row.id_film) }}"
            id_montre_materiau_edit = request.values['id_montre_materiau_edit_html']

            # Mémorise l'id du film dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_montre_materiau_edit'] = id_montre_materiau_edit

            # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom_montre de variable
            valeur_id_montre_selected_dictionnaire = {"value_id_montre_selected": id_montre_materiau_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la fonction materiau_montre_afficher_data
            # 1) Sélection du film choisi
            # 2) Sélection des materiaux "déjà" attribués pour le film.
            # 3) Sélection des materiaux "pas encore" attribués pour le film choisi.
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "materiau_montre_afficher_data"
            data_materiau_montre_selected, data_materiau_montre_non_attribues, data_materiau_montre_attribues = \
                materiau_montre_afficher_data(valeur_id_montre_selected_dictionnaire)

            print(data_materiau_montre_selected)
            lst_data_montre_selected = [item['id_montre'] for item in data_materiau_montre_selected]
            print("lst_data_montre_selected  ", lst_data_montre_selected,
                  type(lst_data_montre_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les materiaux qui ne sont pas encore sélectionnés.
            lst_data_materiau_montre_non_attribues = [item['id_materiau'] for item in data_materiau_montre_non_attribues]
            session['session_lst_data_materiau_montre_non_attribues'] = lst_data_materiau_montre_non_attribues
            print("lst_data_materiau_montre_non_attribues  ", lst_data_materiau_montre_non_attribues,
                  type(lst_data_materiau_montre_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les materiaux qui sont déjà sélectionnés.
            lst_data_materiau_montre_old_attribues = [item['id_materiau'] for item in data_materiau_montre_attribues]
            session['session_lst_data_materiau_montre_old_attribues'] = lst_data_materiau_montre_old_attribues
            print("lst_data_materiau_montre_old_attribues  ", lst_data_materiau_montre_old_attribues,
                  type(lst_data_materiau_montre_old_attribues))

            print(" data data_materiau_montre_selected", data_materiau_montre_selected, "type ", type(data_materiau_montre_selected))
            print(" data data_materiau_montre_non_attribues ", data_materiau_montre_non_attribues, "type ",
                  type(data_materiau_montre_non_attribues))
            print(" data_materiau_montre_attribues ", data_materiau_montre_attribues, "type ",
                  type(data_materiau_montre_attribues))

            # Extrait les valeurs contenues dans la table "t_genres", colonne "intitule_genre"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_genre
            lst_data_materiau_montre_non_attribues = [item['materiau'] for item in data_materiau_montre_non_attribues]
            print("lst_all_materiau gf_edit_materiau_montre_selected ", lst_data_materiau_montre_non_attribues,
                  type(lst_data_materiau_montre_non_attribues))

        except Exception as Exception_edit_materiau_montre_selected:
            code, msg = Exception_edit_materiau_montre_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception edit_materiau_montre_selected : {sys.exc_info()[0]} "
                  f"{Exception_edit_materiau_montre_selected.args[0]} , "
                  f"{Exception_edit_materiau_montre_selected}", "danger")

    return render_template("Montres_materiaux/montres_matriaux_modifier_tags_dropbox.html",
                           data_genres=data_materiau_all,
                           data_montre_selected=data_materiau_montre_selected,
                           data_genres_attribues=data_materiau_montre_attribues,
                           data_genres_non_attribues=data_materiau_montre_non_attribues)

@obj_mon_application.route("/update_materiau_montre_selected", methods=['GET', 'POST'])
def update_materiau_montre_selected():
    if request.method == "POST":
        try:
            # Récupère l'id du film sélectionné
            id_montre_selected = session['session_id_montre_materiau_edit']
            print("session['session_id_montre_materiau_edit'] ", session['session_id_montre_materiau_edit'])

            # Récupère la liste des materiaux qui ne sont pas associés au film sélectionné.
            old_lst_data_materiau_montre_non_attribues = session['session_lst_data_materiau_montre_non_attribues']
            print("old_lst_data_materiau_montre_non_attribues ", old_lst_data_materiau_montre_non_attribues)

            # Récupère la liste des materiaux qui sont associés au film sélectionné.
            old_lst_data_materiau_montre_attribues = session['session_lst_data_materiau_montre_old_attribues']
            print("old_lst_data_materiau_montre_old_attribues ", old_lst_data_materiau_montre_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme materiaux dans le composant "tags-selector-tagselect"
            # dans le fichier "genres_films_modifier_tags_dropbox.html"
            new_lst_str_materiau_montre = request.form.getlist('name_select_tags')
            print("new_lst_str_materiau_montre ", new_lst_str_materiau_montre)

            # OM 2021.05.02 Exemple : Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_materiau_montre_old = list(map(int, new_lst_str_materiau_montre))
            print("new_lst_materiau_montre ", new_lst_int_materiau_montre_old, "type new_lst_materiau_montre ",
                  type(new_lst_int_materiau_montre_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2021.05.02 Une liste de "id_genre" qui doivent être effacés de la table intermédiaire "t_genre_film".
            lst_diff_materiau_delete_b = list(
                set(old_lst_data_materiau_montre_attribues) - set(new_lst_int_materiau_montre_old))
            print("lst_diff_materiau_delete_b ", lst_diff_materiau_delete_b)

            # Une liste de "id_genre" qui doivent être ajoutés à la "t_genre_film"
            lst_diff_materiau_insert_a = list(
                set(new_lst_int_materiau_montre_old) - set(old_lst_data_materiau_montre_attribues))
            print("lst_diff_materiau_insert_a ", lst_diff_materiau_insert_a)

            # SQL pour insérer une nouvelle association entre
            # "fk_film"/"id_film" et "fk_genre"/"id_genre" dans la "t_genre_film"
            strsql_insert_materiau_montre = """INSERT INTO t_montre_materiau (id_montre_materiau, fk_materiau, fk_montre)
                                                    VALUES (NULL, %(value_fk_materiau)s, %(value_fk_montre)s)"""

            # SQL pour effacer une (des) association(s) existantes entre "id_film" et "id_genre" dans la "t_genre_film"
            strsql_delete_materiau_montre = """DELETE FROM t_montre_materiau WHERE fk_materiau = %(value_fk_materiau)s AND fk_montre = %(value_fk_montre)s"""

            with MaBaseDeDonnee() as mconn_bd:
                # Pour le film sélectionné, parcourir la liste des materiaux à INSÉRER dans la "t_genre_film".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_materiau_ins in lst_diff_materiau_insert_a:
                    # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom_montre de variable
                    # et "id_genre_ins" (l'id du genre dans la liste) associé à une variable.
                    valeurs_montre_sel_materiau_sel_dictionnaire = {"value_fk_montre": id_montre_selected,
                                                               "value_fk_materiau": id_materiau_ins}

                    mconn_bd.mabd_execute(strsql_insert_materiau_montre, valeurs_montre_sel_materiau_sel_dictionnaire)

                # Pour le film sélectionné, parcourir la liste des materiaux à EFFACER dans la "t_genre_film".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_materiau_del in lst_diff_materiau_delete_b:
                    # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom_montre de variable
                    # et "id_genre_del" (l'id du genre dans la liste) associé à une variable.
                    valeurs_montre_sel_materiau_sel_dictionnaire = {"value_fk_montre": id_montre_selected,
                                                               "value_fk_materiau": id_materiau_del}

                    # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
                    # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
                    # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
                    # sera interprété, ainsi on fera automatiquement un commit
                    mconn_bd.mabd_execute(strsql_delete_materiau_montre, valeurs_montre_sel_materiau_sel_dictionnaire)

        except Exception as Exception_update_materiau_montre_selected:
            code, msg = Exception_update_materiau_montre_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception update_materiau_montre_selected : {sys.exc_info()[0]} "
                  f"{Exception_update_materiau_montre_selected.args[0]} , "
                  f"{Exception_update_materiau_montre_selected}", "danger")

    # Après cette mise à jour de la table intermédiaire "t_genre_film",
    # on affiche les films et le(urs) genre(s) associé(s).
    return redirect(url_for('montre_materiau_afficher', id_montre_sel=0))


def materiau_montre_afficher_data(valeur_id_montre_selected_dict):
    print("valeur_id_montre_selected_dict...", valeur_id_montre_selected_dict)
    try:

        strsql_montre_selected = """SELECT id_montre, nom_montre, marque_montre, description_montre, photo_montre, prix_montre, GROUP_CONCAT(id_materiau) as MateriauMontre FROM t_montre_materiau
                                        INNER JOIN t_montre ON t_montre.id_montre = t_montre_materiau.fk_montre
                                        INNER JOIN t_materiau ON t_materiau.id_materiau = t_montre_materiau.fk_materiau
                                        WHERE id_montre = %(value_id_montre_selected)s"""

        strsql_materiau_montre_non_attribues = """SELECT id_materiau, materiau FROM t_materiau WHERE id_materiau not in(SELECT id_materiau as idMateriauMontre FROM t_montre_materiau
                                                    INNER JOIN t_montre ON t_montre.id_montre = t_montre_materiau.fk_montre
                                                    INNER JOIN t_materiau ON t_materiau.id_materiau = t_montre_materiau.fk_materiau
                                                    WHERE id_montre = %(value_id_montre_selected)s)"""

        strsql_materiau_montre_attribues = """SELECT id_montre, id_materiau, materiau FROM t_montre_materiau
                                            INNER JOIN t_montre ON t_montre.id_montre = t_montre_materiau.fk_montre
                                            INNER JOIN t_materiau ON t_materiau.id_materiau = t_montre_materiau.fk_materiau
                                            WHERE id_montre = %(value_id_montre_selected)s"""

        # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
        with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
            # Envoi de la commande MySql
            mc_afficher.execute(strsql_materiau_montre_non_attribues, valeur_id_montre_selected_dict)
            # Récupère les données de la requête.
            data_materiau_montre_non_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("materiau_montre_afficher_data ----> data_materiau_montre_non_attribues ", data_materiau_montre_non_attribues,
                  " Type : ",
                  type(data_materiau_montre_non_attribues))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_montre_selected, valeur_id_montre_selected_dict)
            # Récupère les données de la requête.
            data_montre_selected = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_montre_selected  ", data_montre_selected, " Type : ", type(data_montre_selected))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_materiau_montre_attribues, valeur_id_montre_selected_dict)
            # Récupère les données de la requête.
            data_materiau_montre_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_materiau_montre_attribues ", data_materiau_montre_attribues, " Type : ",
                  type(data_materiau_montre_attribues))

            # Retourne les données des "SELECT"
            return data_montre_selected, data_materiau_montre_non_attribues, data_materiau_montre_attribues
    except pymysql.Error as pymysql_erreur:
        code, msg = pymysql_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.Error Erreur dans materiau_montre_afficher_data : {sys.exc_info()[0]} "
              f"{pymysql_erreur.args[0]} , "
              f"{pymysql_erreur}", "danger")
    except Exception as exception_erreur:
        code, msg = exception_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"Exception Erreur dans materiau_montre_afficher_data : {sys.exc_info()[0]} "
              f"{exception_erreur.args[0]} , "
              f"{exception_erreur}", "danger")
    except pymysql.err.IntegrityError as IntegrityError_materiau_montre_afficher_data:
        code, msg = IntegrityError_materiau_montre_afficher_data.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.err.IntegrityError Erreur dans materiau_montre_afficher_data : {sys.exc_info()[0]} "
              f"{IntegrityError_materiau_montre_afficher_data.args[0]} , "
              f"{IntegrityError_materiau_montre_afficher_data}", "danger")


@obj_mon_application.route("/montre_ajouter", methods=['GET', 'POST'])
def montre_ajouter_wtf():
    form = FormWTFAjouterMontres()
    if request.method == "POST":
        print("début")
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion materiaux ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionGenres {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                print("dans form_validate_on_submit")
                nom_montre = form.nom_montre_wtf.data
                marque_montre = form.marque_montre_wtf.data
                description_montre = form.description_montre_wtf.data
                photo_montre = form.photo_montre_wtf.data
                prix_montre = form.prix_montre_wtf.data

                valeurs_insertion_dictionnaire = {"nom_montre": nom_montre, "marque_montre": marque_montre, "description_montre": description_montre, "photo_montre": photo_montre, "prix_montre": prix_montre }
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                #, marque_montre, description_montre, photo_montre, prix_montre
                strsql_insert_montre = """INSERT INTO t_montre (id_montre, nom_montre, marque_montre, description_montre, photo_montre, prix_montre) VALUES (NULL,%(nom_montre)s,%(marque_montre)s,%(description_montre)s,%(photo_montre)s,%(prix_montre)s )"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_montre, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('montre_materiau_afficher', order_by='ASC', id_montre_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_genre_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_genre_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion montre CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("Montres_materiaux/montre_ajouter_wtf.html", form=form)


@obj_mon_application.route("/montre_update", methods=['GET', 'POST'])
def montre_update_wtf():
    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_genre"
    id_montre_update = request.values['id_montre_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateMontres()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "montre_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            nom_montre_update = form_update.nom_montre_update_wtf.data
            marque_montre_update = form_update.marque_montre_update_wtf.data
            description_montre_update = form_update.description_montre_update_wtf.data
            photo_montre_update = form_update.photo_montre_update_wtf.data
            prix_montre_update = form_update.prix_montre_update_wtf.data


            valeur_update_dictionnaire = {"value_id_montre": id_montre_update,
                                          "value_nom_montre": nom_montre_update,
                                          "value_marque_montre": marque_montre_update,
                                          "value_description_montre": description_montre_update,
                                          "value_photo_montre":  photo_montre_update,
                                          "value_prix_montre": prix_montre_update}

            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_montre = """UPDATE t_montre 
                                        SET nom_montre = %(value_nom_montre)s, 
                                        marque_montre = %(value_marque_montre)s, 
                                        description_montre = %(value_description_montre)s, 
                                        photo_montre = %(value_photo_montre)s, 
                                        prix_montre = %(value_prix_montre)s 
                                        WHERE id_montre = %(value_id_montre)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_montre, valeur_update_dictionnaire)

            flash(f"Modifications éffectuées !", "success")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_materiau_update"
            return redirect(url_for('montre_materiau_afficher', order_by="ASC", id_montre_sel=0))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_genre" et "intitule_genre" de la "t_genre"
            str_sql_id_montre = "SELECT id_montre, nom_montre, marque_montre, description_montre, photo_montre, prix_montre FROM t_montre WHERE id_montre = %(value_id_montre)s"
            valeur_select_dictionnaire = {"value_id_montre": id_montre_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_montre, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom_montre materiau" pour l'UPDATE
            data_nom_montre = mybd_curseur.fetchone()

            print("data_nom_montre ", data_nom_montre, " type ", type(data_nom_montre), " nom_montre ",
                  data_nom_montre["nom_montre"])


            # Afficher la valeur sélectionnée dans le champ du formulaire "materiau_update_wtf.html"
            form_update.nom_montre_update_wtf.data = data_nom_montre["nom_montre"]
            form_update.marque_montre_update_wtf.data = data_nom_montre["marque_montre"]
            form_update.description_montre_update_wtf.data = data_nom_montre["description_montre"]
            form_update.photo_montre_update_wtf.data = data_nom_montre["photo_montre"]
            form_update.prix_montre_update_wtf.data = data_nom_montre["prix_montre"]


    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans montre_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans montre_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans montre_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans montre_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("Montres_materiaux/montre_update_wtf.html", form_update=form_update)


@obj_mon_application.route("/montre_delete", methods=['GET', 'POST'])
def montre_delete_wtf():
    data_montre_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_genre"
    id_montre_delete = request.values['id_montre_btn_delete_html']

    # Objet formulaire pour effacer le genre sélectionné.
    form_delete = FormWTFDeleteMontres()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("montre_materiau_afficher", order_by="ASC", id_montre_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "materiaux/materiau_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_montre_delete = session['data_montre_delete']
                print("data_montre_delete ", data_montre_delete)

                flash(f"Effacer la montre de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer genre" qui va irrémédiablement EFFACER le genre
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_montre": id_montre_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_montre = """DELETE FROM t_montre_materiau WHERE fk_montre = %(value_id_montre)s"""
                str_sql_delete_id_montre = """DELETE FROM t_montre WHERE id_montre = %(value_id_montre)s"""
                # Manière brutale d'effacer d'abord la "fk_genre", même si elle n'existe pas dans la "t_genre_film"
                # Ensuite on peut effacer le genre vu qu'il n'est plus "lié" (INNODB) dans la "t_genre_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_montre, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_id_montre, valeur_delete_dictionnaire)

                flash(f"Montre définitivement effacé !!", "success")
                print(f"Montre définitivement effacé !!")

                # afficher les données
                return redirect(url_for('montre_materiau_afficher', order_by="ASC", id_montre_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_montre": id_montre_delete}
            print(id_montre_delete, type(id_montre_delete))

            # Requête qui affiche tous les montres_materiaux qui ont le genre que l'utilisateur veut effacer
            str_sql_montre_materiau_delete = """SELECT t_montre.id_montre, t_montre.nom_montre, t_montre_materiau.id_montre_materiau, t_montre_materiau.fk_materiau, t_materiau.id_materiau, t_materiau.materiau 
                                                FROM t_montre      
                                                LEFT JOIN t_montre_materiau ON t_montre_materiau.fk_montre = t_montre.id_montre
                                                LEFT JOIN t_materiau ON t_montre_materiau.fk_materiau = t_materiau.id_materiau
                                                WHERE fk_montre = %(value_id_montre)s
                                            """

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_montre_materiau_delete, valeur_select_dictionnaire)
            data_montre_delete = mybd_curseur.fetchall()
            print("data_montre_attribue_materiau_delete...", data_montre_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "materiaux/materiau_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_montre_delete'] = data_montre_delete

            # Opération sur la BD pour récupérer "id_genre" et "intitule_genre" de la "t_genre"
            str_sql_id_genre = "SELECT id_montre, nom_montre FROM t_montre WHERE id_montre = %(value_id_montre)s"

            mybd_curseur.execute(str_sql_id_genre, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom_montre genre" pour l'action DELETE
            data_nom_montre = mybd_curseur.fetchone()
            print("data_nom_materiau ", data_nom_montre, " type ", type(data_nom_montre), " materiau ",
                  data_nom_montre["nom_montre"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "materiau_delete_wtf.html"
            form_delete.nom_montre_delete_wtf.data = data_nom_montre["nom_montre"]

            # Le bouton pour l'action "DELETE" dans le form. "materiau_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans montre_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans montre_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans montre_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans montre_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("Montres_materiaux/montre_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_montres_associes=data_montre_delete)


@obj_mon_application.route("/montre_nombre", methods=['GET', 'POST'])
def montre_nombre_wtf():
    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_genre"
    id_montre_nombre = request.values['id_montre_btn_nombre_html']

    # Objet formulaire pour l'UPDATE
    form_nombre = FormWTFNombreMontres()
    try:
        print(" on submit ", form_nombre.validate_on_submit())
        if form_nombre.validate_on_submit():
            # Récupèrer la valeur du champ depuis "montre_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            montre_nombre = form_nombre.nombre_montre_wtf.data

            valeur_nombre_dictionnaire = {"value_id_nombre": id_montre_nombre, "value_nombre": montre_nombre}

            print("valeur_update_dictionnaire ", valeur_nombre_dictionnaire)

            str_sql_nombre_montre = """UPDATE t_nombre 
                                        SET nombre = %(value_nombre)s
                                        WHERE id_nombre = %(value_id_nombre)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_nombre_montre, valeur_nombre_dictionnaire)

            flash(f"Modifications éffectuées !", "success")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_materiau_update"
            return redirect(url_for('montre_materiau_afficher', order_by="ASC", id_montre_sel=0))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_genre" et "intitule_genre" de la "t_genre"
            str_sql_id_nombre = "SELECT id_nombre, nombre FROM t_nombre WHERE id_nombre = %(value_id_nombre)s"
            valeur_select_dictionnaire = {"value_id_nombre": id_montre_nombre}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_nombre, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom_montre materiau" pour l'UPDATE
            data_nombre_montre = mybd_curseur.fetchone()

            print("data_nom_montre ", data_nombre_montre, " type ", type(data_nombre_montre), " nom_montre ",
                  data_nombre_montre["nom_montre"])


            # Afficher la valeur sélectionnée dans le champ du formulaire "materiau_update_wtf.html"
            form_nombre.nombre_montre_wtf.data = data_nombre_montre["nombre"]

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans montre_nombre_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans montre_nombre_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans montre_nombre_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans montre_nombre_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("Montres_materiaux/montre_nombre_wtf.html", form_nombre=form_nombre)