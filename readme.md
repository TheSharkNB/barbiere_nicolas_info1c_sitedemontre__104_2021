Bienvenu dans votre application d'enregistrement de vos montres
---


# Pour faire foncionner cette base de donnée Veuillez suivre les étapes suivantes:
## étape 1 : Télécharger l'interpréteur de language Python
* Rendez-vous à l'adresse suivante pour téléchargé la version 3.9.2 de python --> https://www.python.org/downloads/release/python-392/.



#### IMPORTANT, lors de l'insallation, n'oubliez pas de cocher la case " Add to the path ".
* Téléchargez la version de python selon votre matériel :
  * Windows
  * MacOS
  * linux
  

## étape 2 : Téléchargement du serveur local UwAmp
* rendez-vous ici pour ce faire et téléchargé la version en rapport avec votre matériel --> https://www.uwamp.com/fr/



## étape 3 : Télécharger l'éditeur pycharm sur le lien suivant: https://www.jetbrains.com/fr-fr/pycharm/download/download-thanks.html?platform=windows&code=PCC



## étape 4 : Démarrage du serveur local UwAmp
* Ouvrir le logiciel UwAmp.
* Cliquez sur le bouton " Démarrer " en haut à gauche dans "serveur ", après avoir fait ça, vous retrouverez en haut à droit dans "status" Apache et MySQL démarrer.

#
##
###

##### IMPORTANT, par la suite, l'étape N°4, ci-dessus, sera obligatoire pour faire fonctionner cette application. ! Ne l'oubliez pas !
###
##
#



## étape 5 : Importation du project sur pycharm.
* Dans PyCharm, importer le project https://gitlab.com/TheSharkNB/barbiere_nicolas_info1c_sitedemontre__104_2021.git grace a la fonction import from CVS.
  ou
* extraire le project contenu dans le fichier zip et ouvré le avec pycharm.
* une fois le project ouvert sur pycharm accepter la demande de création de l'environement virtuelle.

## étape 6 : importation de la base de donnés sur notre serveur SQL phpmyadmin.
* dans le répertoire "APP_MONTRES/importation" faites un clic droite sur le fichier "1_ImportationDumpSql.py" puis "Run 1_ImportationDumpSql.py".
  En cas d'erreurs : ouvrir le fichier ".env" à la racine du projet, contrôler les indications de connexion suivante:
* (USER_MYSQL="root") 
* (PASS_MYSQL="root")
* NAME_BD_MYSQL="barbiere_nicolas_info1c_sitedemontre_104_2021"
* NAME_FILE_DUMP_SQL_BD="../database/barbiere_nicolas_info1c_sitedemontre_104_2021.sql"
* Constatez l'apparation de la base de donnée dans le menu de gauche. Elle se nomera "barbiere_nicolas_info1c_sitedemontre_104_2021"
* Si vous constatez que la base n'est pas présente, actualiser la page de votre navigateur internet.

## étape 7 : démarage du serveur flask.
* dans le répertoire racine du projet, ouvrir le fichier "1_run_server_flask.py" et faire clique droite puis un "run" dans la barre d'onglet
* dans la console cliquer sur le lien du serveur local
  ou
* dans votre navigateur internet taper l'adresse suivante http://127.0.0.1:5005/

#
##
###
##### IMPORTANT, si votre navigateur n'arrive pas a joindre le lien ci-dessus veuillez vous référer au point numero 4.
###
##
#


# ATTENTION, une fois le fichier "1_run_server_flask.py" lancé, merci de réduire (pas fermer !) sans plus attendre votre application PyCharm afin d'éviter tout problème de modification du code de votre application. 

# une fois toutes les étapes réussie et sans erreur, vous allez pouvoir profiter de votre application #






